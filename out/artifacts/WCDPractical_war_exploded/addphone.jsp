<%--
  Created by IntelliJ IDEA.
  User: Phạm Dũng
  Date: 5/14/2020
  Time: 8:18 AM
  To change this template use File | Settings | File Templates.
--%>
<form action="PhoneServlet" method="post">
    <input type="text" name="name" placeholder="Name" required/>
    <select id="brand" name="brand">
        <option value="Apple">Apple</option>
        <option value="Samsung">Samsung</option>
        <option value="Nokia">Nokia</option>
        <option value="Other">Other</option>
    </select>
    <input type="number" name="price" placeholder="Price" required/>
    <input type="text" name="description" placeholder="Description" required/>
    <input type="submit" value="Submit"/>
    <input type="reset" value="Reset"/>
</form>
